# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Nextcloud package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Nextcloud 3.14159\n"
"Report-Msgid-Bugs-To: translations\\@example.com\n"
"POT-Creation-Date: 2021-05-12 14:35+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: /collectives-files.js:1
msgid "img"
msgid_plural ".svg"
msgstr[0] ""
msgstr[1] ""

#: /collectives-main.js:2
msgid "seconds"
msgstr ""

#: /collectives/lib/Fs/UserFolderHelper.php:52
#: /collectives/lib/Search/CollectiveProvider.php:58
#: /collectives/lib/Search/CollectiveProvider.php:106
#: /collectives/specialAppInfoFakeDummyForL10nScript.php:2
#: /collectives/lib/Fs/UserFolderHelper.php:52
#: /collectives/lib/Search/CollectiveProvider.php:58
#: /collectives/lib/Search/CollectiveProvider.php:106
#: /collectives/specialAppInfoFakeDummyForL10nScript.php:2
#: /collectives/specialVueFakeDummyForL10nScript.js:21
msgid "Collectives"
msgstr ""

#: /collectives/lib/Search/PageProvider.php:65
#: /collectives/lib/Search/PageProvider.php:113
#: /collectives/lib/Search/PageProvider.php:65
#: /collectives/lib/Search/PageProvider.php:113
msgid "Collective Pages"
msgstr ""

#: /collectives/lib/Search/PageProvider.php:106
#: /collectives/lib/Search/PageProvider.php:106
msgid "in {collective}"
msgstr ""

#: /collectives/lib/Service/CollectiveService.php:104
#: /collectives/lib/Service/CollectiveService.php:104
#, php-format
msgid "Created collective \"%s\" for existing circle."
msgstr ""

#: /collectives/specialAppInfoFakeDummyForL10nScript.php:3
#: /collectives/specialAppInfoFakeDummyForL10nScript.php:3
msgid "A place for activist and community projects to build shared knowledge"
msgstr ""

#: /collectives/specialAppInfoFakeDummyForL10nScript.php:4
#: /collectives/specialAppInfoFakeDummyForL10nScript.php:4
msgid ""
"Collectives is a Nextcloud App for activist and community projects to "
"organize together.\n"
"Come and gather in collectives to build shared knowledge.\n"
"\n"
"* 👥 **Collective and non-hierarchical workflow by heart**: Collectives are\n"
"  tied to a [Nextcloud Circle](https://github.com/nextcloud/circles) and\n"
"  owned by the collective.\n"
"* 📝 **Collaborative page editing** like known from Etherpad thanks to the\n"
"  [Text app](https://github.com/nextcloud/text).\n"
"* 🔤 **Well-known [Markdown](https://en.wikipedia.org/wiki/Markdown) "
"syntax**\n"
"  for page formatting.\n"
"\n"
"## Installation\n"
"\n"
"In your Nextcloud instance, simply navigate to **»Apps«**, find the\n"
"**»Circles«** and **»Collectives«** apps and enable them."
msgstr ""

#: /collectives/templates/error.php:4
#: /collectives/templates/error.php:4
msgid "Error: Missing apps"
msgstr ""

#: /collectives/templates/error.php:9
#: /collectives/templates/error.php:9
msgid "The following dependency apps are missing: "
msgstr ""

#: /collectives/templates/error.php:27
#: /collectives/templates/error.php:27
msgid "Please ask the administrator to enable these apps."
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:1
msgid "Could not fetch page ${pageId}"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:2
msgid "Deleted collectives"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:3
#: /collectives/specialVueFakeDummyForL10nScript.js:38
msgid "Restore"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:4
msgid "Delete permanently"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:5
msgid "Permanently delete collective »{collective}«"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:6
msgid "Delete corresponding circle along with the collective?"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:7
msgid "Cancel"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:8
msgid "Only collective"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:9
msgid "Collective and circle"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:10
msgid "Last edited by {user}"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:11
msgid "Select a collective"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:12
msgid "Delete"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:13
msgid "Create new collective"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:14
msgid "Create collective for existing circle"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:15
msgid "Add emoji"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:16
msgid "New collective name"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:17
msgid "Select circle..."
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:18
msgid "Title"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:19
msgid "Done"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:20
msgid "Edit"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:22
msgid "Could not rename the page"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:23
msgid "Delete page"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:24
msgid "Page deleted"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:25
msgid "Could not delete the page"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:26
#: /collectives/specialVueFakeDummyForL10nScript.js:34
msgid "Add a subpage"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:27
#: /collectives/specialVueFakeDummyForL10nScript.js:35
msgid "New Page"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:28
#: /collectives/specialVueFakeDummyForL10nScript.js:36
msgid "Could not create the page"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:29
msgid "Current version"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:30
msgid "No other versions available"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:31
msgid "After editing you can find old versions of the page here."
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:32
msgid "Could not get page versions"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:33
#: /collectives/src/util/xmlToVersionsList.js:29
msgid "%n byte"
msgid_plural "%n bytes"
msgstr[0] ""
msgstr[1] ""

#: /collectives/specialVueFakeDummyForL10nScript.js:37
msgid "Restore this version"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:39
msgid "Reverted {page} to revision {timestamp}."
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:40
msgid "Failed to revert {page} to revision {timestamp}."
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:41
msgid "No collective selected"
msgstr ""

#: /collectives/specialVueFakeDummyForL10nScript.js:42
msgid "Select a collective or create a new one on the left."
msgstr ""
