# Willkommen in eurem neuen Kollektiv

*Kommt vorbei, organisiert euch und teilt euer Wissen!*


### 🐾 Ladet eure Genoss:innen ins Kollektiv ein

Mitglieder können in der [Circle App](/index.php/apps/circles/) verwaltet werden.

### 🌱 Füllt euer Kollektiv mit Leben

Erstellt Seiten und teilt euer wertvolles Wissen.

### 🛋️ Verändert die Startseite und fühlt euch wie Zuhause

Wechselt in den Editier-Modus um loszulegen. ↗️


## Auch gut zu wissen

* Verlinkt lokale Seiten, indem ihr Text markiert und "Datei verknüpfen" wählt.
* Mehrere Menschen können gleichzeitig die selbe Seite bearbeiten.
* Findet mehr über diese App heraus und schaut in die [Dokumentation](https://collectivecloud.gitlab.io/collectives/).
* Fragt [die Community](https://help.nextcloud.com/c/apps/collectives/174) um Hilfe, wenn ihr weitere Fragen habt.
