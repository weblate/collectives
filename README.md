# Collectives

Collectives is a Nextcloud App for activist and community projects to
organize together. Come and gather in collectives to build shared knowledge.

* 👥 **Collective and non-hierarchical workflow by heart**: Collectives are
  tied to a [Nextcloud Circle](https://github.com/nextcloud/circles) and
  owned by the collective.
* 📝 **Collaborative page editing** like known from Etherpad thanks to the
  [Text app](https://github.com/nextcloud/text).
* 🔤 **Well-known [Markdown](https://en.wikipedia.org/wiki/Markdown) syntax**
  for page formatting.

![Screenshot of Nextcloud Collectives Version 0.2.1](https://gitlab.com/collectivecloud/collectives/-/raw/main/docs/static/images/screenshot.png)

## Installation

In your Nextcloud instance, simply navigate to **»Apps«**, find the
**»Circles«** and **»Collectives«** apps and enable them.

## Documentation and help

Take a look at our [online documention](https://collectivecloud.gitlab.io/collectives/).

Also, don't hesitate to ask [the community](https://help.nextcloud.com/c/apps/collectives/174)
for help in case of questions.

## Developer documentation

Documentation for developers can be found at [DEVELOPING.md](DEVELOPING.md).

## Maintainers

* Azul and Jonas <collectivecloud@systemli.org>

## Licence

AGPL v3 or later. See [COPYING](COPYING) for the full licence text.

The app logo and corresponding icons were contributed by Jörg Schmidt from
Institut für Gebrauchsgrafik <info@institut.gebrauchsgrafik.org>.
